﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UploadImages
{
    public class Logger
    {

        private string m_exePath = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="logMessage"></param>
        /// <param name="logtype"></param>
        public static void LogWrite(string logMessage, string logtype)
        {
            string m_exePath = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "/Logs/" + DateTime.Now.ToString("yyyy-MM-dd");
            if (!System.IO.Directory.Exists(m_exePath))
            {
                System.IO.Directory.CreateDirectory(m_exePath);
            }
            try
            {
                using (StreamWriter w = File.AppendText(m_exePath + "\\" + string.Format(logtype + "_log.txt")))
                {
                    Log(logMessage, w);
                }
            }
            catch (Exception ex)
            {
                Logger.LogWrite(ex.Message, "Error_LogWrite");
            }
        }

        /// <summary>
        /// Write Logs
        /// </summary>
        /// <param name="logMessage"></param>
        /// <param name="txtWriter"></param>
        public static void Log(string logMessage, TextWriter txtWriter)
        {
            try
            {
                txtWriter.Write("\r");  //\r\n 换行回车
                txtWriter.WriteLine(" {0} {1}: {2} ", DateTime.Now.ToLongDateString(), DateTime.Now.ToLongTimeString(), logMessage);
            }
            catch (Exception)
            {
                //Logger.LogWrite(ex.Message, "Error_LogWrite");
            }
        }

    }
}
