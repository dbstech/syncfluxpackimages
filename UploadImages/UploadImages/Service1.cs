﻿using Qiniu.Http;
using Qiniu.IO;
using Qiniu.IO.Model;
using Qiniu.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UploadImages
{
    public partial class Service1 : ServiceBase
    {
        string imagePath = System.Configuration.ConfigurationManager.AppSettings["ImagesPath"].ToString();
        int savedays= int.Parse(System.Configuration.ConfigurationManager.AppSettings["SaveDays"].ToString());
        int DelayShutdown = int.Parse(System.Configuration.ConfigurationManager.AppSettings["DelayShutdown"].ToString());

        int Hours = int.Parse(System.Configuration.ConfigurationManager.AppSettings["RunTimeHours"].ToString());
        int Minutes = int.Parse(System.Configuration.ConfigurationManager.AppSettings["RunTimeMinutes"].ToString());
        int Seconds = int.Parse(System.Configuration.ConfigurationManager.AppSettings["RunTimeSeconds"].ToString());

        int ReStartTime = int.Parse(System.Configuration.ConfigurationManager.AppSettings["ReStartDateTime"].ToString());

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            //如果重启时间 17>=30 才执行删除旧照片和上传图片
            if (int.Parse(DateTime.Now.ToString("HHmm")) - ReStartTime > 0 )
            {
                //先执行一次
                Logger.LogWrite(" 服务自动启动--开机 ", "机器启动日志");
                // 删除1周之前的所有图片
                DeleteFile(imagePath, savedays);
                // 处理文件; 上传文件
                processimages();
            }

            System.Timers.Timer TimeForminute = new System.Timers.Timer();
            TimeForminute.Interval = 1000;
            TimeForminute.Elapsed += new System.Timers.ElapsedEventHandler(SearchPic);//到达时间的时候执行事件； 
            TimeForminute.AutoReset = true;//设置是执行一次（false）还是一直执行(true)； 
            TimeForminute.Enabled = true;//是否执行System.Timers.Timer.Elapsed事件； 
        }

        protected override void OnStop()
        {
        }

        /// <summary>
        /// 遍历搜索图片
        /// </summary>
        /// <param name="file_name"></param>
        /// <returns></returns>
        public void SearchPic(object source, System.Timers.ElapsedEventArgs e)
        {
            int intHour = e.SignalTime.Hour;
            int intMinute = e.SignalTime.Minute;
            int intSecond = e.SignalTime.Second;

            // 21:10 重试一次执行完成后，关机
            if (intHour == Hours && intMinute == Minutes && intSecond == Seconds) 
            {
                processimages();
                // 删除1周之前的所有图片
                DeleteFile(imagePath, savedays);
                // 执行后关闭计算机
                Thread.Sleep(1000* DelayShutdown); //延时关机 秒
                Logger.LogWrite(" 服务自动启动--关机 ", "机器启动日志");
                ComputerControl.DoExitWindows(ComputerControl.ExitWindows.ShutDown);
            }
       
        }

        public void processimages() {
            try
            {
                DirectoryInfo dir = new DirectoryInfo(imagePath);
                FileInfo[] files = dir.GetFiles("*.jpg");
                int successful = 0;
                int current = 0;

                foreach (FileInfo f in files)
                {
                    current++;
                    if (UploadPic(f.FullName, f.Name.Replace(".jpg", "")))
                    {
                        successful++;
                        // 写日志
                        Logger.LogWrite("  第 " + current.ToString() + "文件 上传成功, 图片名称: " + f.FullName, "日志详情");
                    }
                }
                // 写日志
                Logger.LogWrite(" 本次同步的图片, 总数量: " + files.Count().ToString() + " 成功数量: " + successful.ToString() + " 失败数量: " + (files.Count() - successful).ToString(), "日志详情");
            }
            catch (Exception ex)
            {
                Logger.LogWrite(" 本次同步的图片失败:" + ex.Message, "处理失败");
            }
        }
       

        public bool UploadPic(string path, string file_name)
        {

            string coon = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            //读取没有上传图片订单信息
            SqlConnection sqlCnt = new SqlConnection(coon);
            SqlCommand cmd = sqlCnt.CreateCommand();
            cmd.CommandType = CommandType.Text;
            DataTable dt = new DataTable();
            try
            {
                sqlCnt.Open();
                cmd.CommandText = "SELECT carrierid from [naky].[dbo].[orders_pack] where status=4 and syc_flag=0 and attchment_name='" + file_name + "'";
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
            }
            catch { }
            finally
            {
                sqlCnt.Close();
            }
            if (dt.Rows.Count > 0)
            {
                string shipment_number = dt.Rows[0]["carrierid"].ToString();

                if (UploadFile(path, shipment_number + "-package-photo.jpg"))
                {

                    try
                    {
                        FileInfo file = new FileInfo(path);
                        file.Delete();
                    }
                    catch { }
                    sqlCnt = new SqlConnection(coon);
                    cmd = sqlCnt.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    try
                    {
                        sqlCnt.Open();
                        cmd.CommandText = "UPDATE [naky].[dbo].[orders_pack] set syc_flag=1 where carrierid='" + shipment_number + "'";
                        int num = cmd.ExecuteNonQuery();
                    }
                    catch { }
                    finally
                    {
                        sqlCnt.Close();
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// UPload file to qiniu
        /// </summary>
        /// <param name="localFile"></param>
        /// <param name="saveKey"></param>
        private bool UploadFile(string localFile, string saveKey)
        {
            try
            {
                Mac mac = new Mac("ueeartkuCYpdnelPRRGV0GpFvOna3s3tYIGcDka5", "3MshWxAPPzbDODGnOzOiELEQgputwZ6lBbMXoyb6");
                PutPolicy putPolicy = new PutPolicy();
                putPolicy.Scope = "tracking-labels:" + saveKey;
                putPolicy.SetExpires(3600000);
                string jstr = Newtonsoft.Json.JsonConvert.SerializeObject(putPolicy);
                string token = Auth.CreateUploadToken(mac, jstr);
                UploadManager um = new UploadManager();
                HttpResult result = um.UploadFile(localFile, saveKey, token);
                if (result.Code == 200)
                {
                    return true;
                }
            }
            catch { }
            return false;
        }

        private void DeleteFile(string fileDirect, int saveDay)
        {
            try
            {
                DateTime nowTime = DateTime.Now;
                string[] files = Directory.GetFiles(fileDirect, "*.jpg", SearchOption.AllDirectories);
                foreach (string file in files)
                {
                    FileInfo fileInfo = new FileInfo(file);
                    TimeSpan t = nowTime - fileInfo.LastWriteTime;//.CreationTime;
                    int day = t.Days;
                    if (day > saveDay)
                    {
                        File.Delete(file);
                        Logger.LogWrite(" 删除过期图片文件数:" + file.ToString(), "机器启动日志");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogWrite(" 删除过期图片文件失败:" + ex.Message, "机器启动日志");
            }

        }
    }


  

    public class ComputerControl {

        private const int SE_PRIVILEGE_ENABLED = 0x00000002;
        private const int TOKEN_QUERY = 0x00000008;
        private const int TOKEN_ADJUST_PRIVILEGES = 0x00000020;
        private const string SE_SHUTDOWN_NAME = "SeShutdownPrivilege";

        [Flags]
        public enum ExitWindows : uint
        {
            LogOff = 0x00, //注销
            ShutDown = 0x01, //关机
            Reboot = 0x02, //重启
            Force = 0x04,
            PowerOff = 0x08,
            ForceIfHung = 0x10
        }

        [Flags]
        private enum ShutdownReason : uint
        {
            MajorApplication = 0x00040000,
            MajorHardware = 0x00010000,
            MajorLegacyApi = 0x00070000,
            MajorOperatingSystem = 0x00020000,
            MajorOther = 0x00000000,
            MajorPower = 0x00060000,
            MajorSoftware = 0x00030000,
            MajorSystem = 0x00050000,
            MinorBlueScreen = 0x0000000F,
            MinorCordUnplugged = 0x0000000b,
            MinorDisk = 0x00000007,
            MinorEnvironment = 0x0000000c,
            MinorHardwareDriver = 0x0000000d,
            MinorHotfix = 0x00000011,
            MinorHung = 0x00000005,
            MinorInstallation = 0x00000002,
            MinorMaintenance = 0x00000001,
            MinorMMC = 0x00000019,
            MinorNetworkConnectivity = 0x00000014,
            MinorNetworkCard = 0x00000009,
            MinorOther = 0x00000000,
            MinorOtherDriver = 0x0000000e,
            MinorPowerSupply = 0x0000000a,
            MinorProcessor = 0x00000008,
            MinorReconfig = 0x00000004,
            MinorSecurity = 0x00000013,
            MinorSecurityFix = 0x00000012,
            MinorSecurityFixUninstall = 0x00000018,
            MinorServicePack = 0x00000010,
            MinorServicePackUninstall = 0x00000016,
            MinorTermSrv = 0x00000020,
            MinorUnstable = 0x00000006,
            MinorUpgrade = 0x00000003,
            MinorWMI = 0x00000015,
            FlagUserDefined = 0x40000000,
            FlagPlanned = 0x80000000
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        private struct TokPriv1Luid
        {
            public int Count;
            public long Luid;
            public int Attr;
        }

        [DllImport("kernel32.dll", ExactSpelling = true)]
        private static extern IntPtr GetCurrentProcess();

        [DllImport("advapi32.dll", ExactSpelling = true, SetLastError = true)]
        private static extern bool OpenProcessToken(IntPtr h, int acc, ref IntPtr phtok);

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool LookupPrivilegeValue(string host, string name, ref long pluid);

        [DllImport("advapi32.dll", ExactSpelling = true, SetLastError = true)]
        private static extern bool AdjustTokenPrivileges(IntPtr htok, bool disall, ref TokPriv1Luid newst, int len, IntPtr prev, IntPtr relen);

        [DllImport("user32.dll")]
        private static extern bool ExitWindowsEx(ExitWindows uFlags, ShutdownReason dwReason);

        /// <summary>
        /// 关机、重启、注销windows
        /// </summary>
        /// <param name="flag"></param>
        public static void DoExitWindows(ExitWindows flag)
        {
            TokPriv1Luid tp;
            IntPtr hproc = GetCurrentProcess();
            IntPtr htok = IntPtr.Zero;
            OpenProcessToken(hproc, TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, ref htok);
            tp.Count = 1;
            tp.Luid = 0;
            tp.Attr = SE_PRIVILEGE_ENABLED;
            LookupPrivilegeValue(null, SE_SHUTDOWN_NAME, ref tp.Luid);
            AdjustTokenPrivileges(htok, false, ref tp, 0, IntPtr.Zero, IntPtr.Zero);
            ExitWindowsEx(flag, ShutdownReason.MajorOther);
        }
    }
}
