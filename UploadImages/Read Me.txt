1、机器上必须安装.net Framework 框架
 <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.5.1"/>
2、开机自动启动服务;
3、每天下班后5:30 做一次图片上传，1分钟后自动关机；
4、所有图片只保留最近一周的，超过1周未上传的图片自动删除（就是解决重复拍照生成的图片)
5、自动记录日志

6、
<configuration>
    <startup> 
        <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.5.1"/>
    </startup>
  <connectionStrings>
    <add name="DefaultConnection" providerName="System.Data.SqlClient" connectionString="Data Source=119.28.61.61,1499;initial catalog=naky;persist security info=True;user id=sa;password=Tokyo-20160822-ALLDB;"/>
  </connectionStrings>
  <appSettings>
    <add key="timeForMinute" value="15"/> 
    <add key="ImagesPath" value="C:\Program Files (x86)\Aladdin\WMS_CLIENT_V431_aladin\photos\temp2\"/>
    <add key="RunTimeHours" value="17"/> <!--运行时间 小时-->
    <add key="RunTimeMinutes" value="40"/> <!--运行时间 分钟-->
    <add key="RunTimeSeconds" value="00"/> <!--运行时间 秒-->
  </appSettings>
</configuration>